package com.enclave.leo;

/**
 * Created by leo on 10/9/15.
 */
/*
Copyright 2011 Rodion Gorkovenko
This file is a part of FREJ
(project FREJ - Fuzzy Regular Expressions for Java - http://frej.sf.net)
FREJ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
FREJ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with FREJ.  If not, see <http://www.gnu.org/licenses/>.
*/

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Class represents fuzzy regular expression at whole.
 *
 * Pattern of fuzzy regexp is passed as string to constructor.
 *
 * Then any string could be checked against this regexp with the
 * help of match, matchFromStart or presentInSequence methods.
 *
 * After matching it is possible to receive replacement for matched
 * region via getReplacement method.
 *
 * Few more auxiliary methods provided for handling parts of original
 * string and result.
 *
 * @author Rodion Gorkovenko
 */
public final class Regex {


    private enum CharType {
        SEPARATOR, DIGIT, LETTER, ALLOWED_PUNCT
    } // CharType

    private Fuzzy fuzzy = new Fuzzy();
    private Elem root;
    String[] exprs;
    private double matchResult;
    // Save presented string
    private String original;
    private String replaceResult;
    private String allowedPunct = ".-";
    private double threshold = fuzzy.threshold;
    Map<String, Elem> subs = new HashMap<String, Elem>();


    /**
     * Creates new regular expression (builds it as a tree of elements) from
     * presented pattern. Behavior is undefined if pattern is incorrect.
     */
    public Regex(String pattern) {
        String ssubs[];
        pattern = fixPattern(pattern);
        ssubs = pattern.split("::");
        for (int i = ssubs.length - 1; i >= 0; i--) {
            int p;
            if (i > 0) {
                for (p = 0; Character.isLetterOrDigit(ssubs[i].charAt(p)); p++);
            } else {
                p = 0;
            } // else
            subs.put(ssubs[i].substring(0, p), parse(ssubs[i].substring(p).replaceAll("\\s+", "")));
        } // for
        root = subs.get("");
    } // Regex


    /**
     * Creates new regular expression from presented pattern, specifying
     * also settings of threshold value and allowed punctuation marks.
     */
    public Regex(String pattern, double threshold) {
        this(pattern);
        if (threshold >= 0) {
            setThreshold(threshold);
        } // if
    } // Regex

    //TODO Treat '-' and '.' as normal character
    /**
     * Change domain name to pattern: special char '-' '.'
     * @param pattern
     * @return
     */
    private String fixPattern(String pattern) {
        StringBuilder b = new StringBuilder();

        for (int i = 0; i < pattern.length(); i++) {
            char c = pattern.charAt(i);
            b.append(c);
        } // for

//        return b.toString();
        pattern = "enclave,enclaveot,enclaveit";
        return pattern;
    } // fixPattern


    private Elem parse(String pattern) {
        Elem retVal;
        retVal = new Any(this, parseList(pattern));
        return retVal;
    } // parse


    private Elem[] parseList(String pattern) {
        String patternList[];
        patternList = pattern.split(",");
        Elem[] list = new Elem[patternList.length];
        for (int i = 0; i < patternList.length; i++) {
            System.out.println("Create Token");
            list[i] = new Token(this, patternList[i]);
        }
        return list;
    } // parseList


    /**
     * Check whether presented string matches with this regexp with all tokens.
     * @return true or false depending on quality of best matching variant.
     */
    public boolean match(String present) {
//        splitTokens(present);
        exprs[0] = present;
        matchResult = root.matchAt(0);
        if (matchResult > threshold || root.getMatchLen() != exprs.length) {
            return false;
        } // if
        replaceResult = root.getReplacement();
        return true;
    } // match


    /**
     * Returns result of the last match. Result is strongly linked to "distance"
     * between strings being fuzzy matched, i.e. it is roughly count of
     * dissimilarities divided by length of matched region.
     *
     * For example "Free" and "Frej" match result is 0.25 while "Bold" and "Frej"
     * gives 1.0.
     * @return measure of dissimilarity, 0 means exact match.
     */
    public double getMatchResult() {
        return matchResult;
    } // getMatchResult


    /**
     * Gives replacement string which is generated after successful match
     * according to rules specified in regexp pattern.
     * @return replacement as a string.
     */
    public String getReplacement() {
        return replaceResult;
    } // getReplacement


    /**
     * Returns value of threshold used in matching methods to decide whether matching result
     * signifies match or mismatch. By default equals to frej.Fuzzy.threshold.
     */
    public double getThreshold() {
        return threshold;
    } // getThreshold


    /**
     * Sets value of threshold used in matching methods to decide whether matching result
     * signifies match or mismatch. By default equals to frej.Fuzzy.threshold.
     */
    public void setThreshold(double t) {
        threshold = t;
    } // setThreshold

    /**
     * split input string to arrays base on charType
     * @param expr: input string
     */
    private void splitTokens(String expr) {
        //tokenList save exprs after split
        List<String> tokenList = new LinkedList<String>();
        StringBuilder s = new StringBuilder();
        CharType prevCharClass, charClass;

        original = expr;

        prevCharClass = CharType.SEPARATOR;
        for (int i = 0; i <= expr.length(); i++) {
            char c;

            try {
                c = expr.charAt(i);
            } catch (IndexOutOfBoundsException e) {
                c = 0;
                charClass = CharType.SEPARATOR;
            } // catch

            charClass = Character.isLetter(c) ? CharType.LETTER :
                    Character.isDigit(c) ? CharType.DIGIT :
                            allowedPunct.indexOf(c) >= 0 ? CharType.ALLOWED_PUNCT : CharType.SEPARATOR;

            if ((prevCharClass != charClass || charClass == CharType.ALLOWED_PUNCT) && s.length() > 0) {
                tokenList.add(s.toString());
                s.setLength(0);
            } // if

            prevCharClass = charClass;

            if (charClass != CharType.SEPARATOR) {
                s.append(c);
            } // if

        } // for

        // tokenList to array with size of tokenList
        exprs = tokenList.toArray(new String[tokenList.size()]);
    } // splitTokens


    Fuzzy getFuzzy() {
        return fuzzy;
    }

} // class FuzzyRegex