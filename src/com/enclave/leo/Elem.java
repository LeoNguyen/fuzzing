package com.enclave.leo;

/**
 * Created by leo on 10/9/15.
 */
abstract class Elem {

    Regex owner;
    int matchStart, matchLen;
    Elem[] children;
    String replacement, matchReplacement;


    Elem(Regex owner) {
        this.owner = owner;
    } // FuzzyRegexElem


    abstract double matchAt(int i);


    int getMatchLen() {
        return matchLen;
    } // getMatchLen


    String childrenString(String prefix, String suffix) {
        StringBuilder s = new StringBuilder(prefix);

        for (int i = 0; i < children.length; i++) {
            s.append(children[i].toString());
            s.append(',');
        } // for

        s.deleteCharAt(s.length() - 1);
        s.append(suffix);

        return s.toString();
    } // childrenString


    String getReplacement() {
        System.out.println("Elem.getReplacement");
        StringBuilder s = new StringBuilder();

        if (replacement == null) {
            return getMatchReplacement();
        } else {
            System.out.println("Do nothing");
        }

        return s.toString();
    } //getReplacement


    String getMatchReplacement() {
        StringBuilder s = new StringBuilder();
        if (matchReplacement != null) {
            return matchReplacement;
        } // if

        for (int i = 0; i < matchLen; i++) {
            s.append(owner.exprs[matchStart + i]);
            s.append(' ');
        } // for
        if (s.length() > 0) {
            s.deleteCharAt(s.length() - 1);
        } // if

        return s.toString();
    } // getMatchReplacement


    @Override
    public String toString() {
        return (replacement != null ? "|" + replacement : "");
    } // toString


} // class FuzzyRegexElem
