package com.enclave.leo;

/**
 * Created by leo on 10/9/15.
 */
final class Any extends Elem {


    Any(Regex owner, Elem... elems) {
        super(owner);
        children = elems;
    } // FuzzyRegexAny


    @Override
    double matchAt(int i) {
        double bestResult = Double.POSITIVE_INFINITY;
        int bestNum = -1;

        matchStart = i;
        matchLen = 0;

        for (int j = 0; j < children.length; j++) {
            double cur;
            cur = children[j].matchAt(i);
            if (cur < bestResult || cur == bestResult && children[j].getMatchLen() > matchLen) {
                bestNum = j;
                bestResult = cur;
                matchLen = children[j].getMatchLen();
            } // if
        } // for
        matchReplacement = (bestNum >= 0) ? children[bestNum].getReplacement() : null;

        return bestResult;
    } // matchAt


    @Override
    public String toString() {
        return childrenString("(^", ")") + super.toString();
    } // toString


} // class FuzzyRegexAny
