package com.enclave.leo;

/**
 * Created by leo on 10/9/15.
 */
/*
Copyright 2011 Rodion Gorkovenko
This file is a part of FREJ
(project FREJ - Fuzzy Regular Expressions for Java - http://frej.sf.net)
FREJ is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
FREJ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with FREJ.  If not, see <http://www.gnu.org/licenses/>.
*/


final class Token extends Elem {


    private String token;
    // cut down expr to token length
    private boolean partial = false;

    private Fuzzy fuzzy;


    Token(Regex owner, String token) {
        super(owner);
        fuzzy = owner.getFuzzy();
        this.token = token;
//        changePattern(token);
    } // FuzzyRegexToken


    @Override
    double matchAt(int i) {

        matchStart = i;
        matchLen = 0;

        if (i >= owner.exprs.length) {
            return Double.POSITIVE_INFINITY;
        } // if

        fuzzy.similarity(owner.exprs[i], token);

        matchLen = 1;

        return fuzzy.result;
    } // matchAt


    @Override
    String getReplacement() {
        if (replacement == null) {
//            return partial ? getMatchReplacement() : token;
            System.out.println("It's null");
            return token;
        } // if

        System.out.println("Call super");
        return super.getReplacement();
    } //getReplacement


    @Override
    public String toString() {
        return token + super.toString();
    } // toString


//    void changePattern(String pattern) {
//        if (pattern == null || pattern.charAt(pattern.length() - 1) != '*') {
//            token = pattern;
//            partial = false;
//        } else {
//            this.token = pattern.substring(0, pattern.length() - 1);
//            partial = true;
//        } // else
//    } // changePattern


} // class FuzzyRegexToken
